/*
	Activity:

	Create an array of objects called courses:

		Each object should have the following fields:

		id -string
		name - string
		description - string
		price - number
		isActive - boolean

	The array should have a minimum of 4 objects.

	//Create
		-Create an arrow function called addCourse which allows us to add a new object into the array. This function should receive the following the data:
			-id,
			-name,
			-description,
			-price
			-isActive
		-This function should be able to show an alert after adding into the array:
			"You have created <nameOfCourse>. Its price is <priceOfCourse>"
		-push method (to push an object in the array of objects)

	//Retrieve/Read

		-Create an arrow function getSingleCourse which allows us to find a particular course by the providing the course's id.

			-Show the details of the found course in the console.
			-return the found course
			- find method

		-Create an arrow function getAllCourses which is able to show all of the items/objects in the array in our console.

			-return the courses array
			- any method (so long as you can return all the courses)

	//Update

		-Create an arrow function called archiveCourse which is able to update/re-assign the isActive property of a particular course, update the isActive property to false. This function should be able to receive the particular index number of the item as an argument.

			-show the updated course in the console.
			- index & dot notation and assignment operator

	
	//Delete
		-Create an arrow function called deleteCourse which is able to delete the last course object in the array.
		-pop method


	Stretch Goals (Added challenge only/No need to accomplish both):

	Create an arrow function which can show all of the active courses only. Show the active courses in the console. See .filter()
	- you may store the result of filter method in a variable

	In the archiveCourse function, the function should instead receive the name of the course and use the name to find the index number of the course.  See findIndex()
	- - you may store the result of filter method in a variable

*/

let courses = [

	{
		id: "1",
		name: "Python 101",
		description: "Learn the basics of python.",
		price: 15000,
		isActive: true

	},
	{
		id: "2",
		name: "CSS 101",
		description: "Learn the basics of CSS.",
		price: 10500,
		isActive: true

	},
	{
		id: "3",
		name: "CSS 102",
		description: "Learn an advanced CSS.",
		price: 15000,
		isActive: false

	},
	{
		id: "4",
		name: "PHP-Laravel 101",
		description: "Learn the basics of PHP and its Laravel framework.",
		price: 20000,
		isActive: true

	}


]



// No 1 Create

const addCourse = (id, name, description, price, isActive) =>{
	let data = {
		id : id,
		name : name, 
		description : description,
		price : price,
		isActive : isActive
	}
	//	console.log(data);
	courses.push(data);
	alert(`You have created ${name}. Its price is ${price}.`);
}

addCourse("1", "Cooking Course", "How to Cook", 1000, true);


// No 2 Read

const getSingleCourse = (id) => {

	// Inner function

	//Single line block doesnt need return keyword
	// let foundCourse = courses.find( (course) => course.id === id);
	
	//Multi line block need return keyword
	let foundCourse = courses.find((course) => {
		//console.log(course);
		return course.id === id
	})

	return foundCourse
}


 console.log(getSingleCourse("2"));


// No 3 Read

const getAllCourses = () => {
	// console.log(courses);

	return courses;
}


console.log(getAllCourses());

console.log(courses);


// No 4 Update


const archiveCourse = (i) => {
	//console.log(courses);

	//courses[i];
	//console.log(courses[i]);
	//console.log(courses[i].isActive);
	courses[i].isActive = false;
	console.log(courses[i].isActive);
}

archiveCourse(1);

// No 5 Delete

const deleteCourse = () =>{
	courses.pop();
	console.log(courses);
}

 deleteCourse();

// Stretch Goals

	// Show active courses
	const getActiveCourses = () => {
		let foundCourses = courses.filter ((course) =>{
			// console.log(course); // objects from courses array
			return course.isActive === true
		})

		return foundCourses
	}

console.log(getActiveCourses());

	// findIndex() to return the index number of the first element which passes the test/ condition

const archiveCourseName = (name) => {
	let indexNum = courses.findIndex(course => {
		// console.log(course); // each object from courses array

		return course.name === name
	})
	// console.log(indexNum); // 3

	//object targetted
	console.log(courses[indexNum]);

	//changing to false
	courses[indexNum].isActive = false;
	console.log(courses[indexNum]);
}

archiveCourseName("PHP-Laravel 101");